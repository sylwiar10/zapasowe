# **Curriculum vitae**
## Anna Kopeć
- E-mail: ania.kopec@gmail.com
- Telefon: 790 712 112

***
### Podsumowanie
Jestem studentką Uniwersytetu Adama Mickiewicza w Lublinie na profilu lingwistycznym. Od ponad dwóch jestem wolontariujuszką w Fundacji Uśmiechu. Płynnie posługuję się językiem angielskim oraz francuskim oraz na poziomie komunikatywnym - językiem hiszpańskim. Chciałabym poszerzać swoje umiejętności językowe i rozwijać swoje zainteresowania.

> "Prawdziwym motorem postępu jest innowacyjność - Bill Gates.
>

***
### Doświadczenie
- wolontariuszka Fundacji Uśmiechu 2019 - obecnie
- szkoła językowa Lingua w Warszawie - 2018 - obecnie
***
### Wykształcenie
- XV Liceum Ogólnokształcące im. Joachyima Lewelka, Lublin, 2015 - profil humanistyczny
- Uniwersytetu Adama Mickiewicza w Lublinie, Lublin 2015-aktualnie - profil lingwistyka stosowana
***
### Umiejętności

| Rodzaj      | Poziom         |
| ------------- |:-------------:|
| Pakiet Office | zaawansowany|
| Językiem angielski | C1 |
| Język francuski | C1|
| Język hiszpański | B2|

### Zainteresowania
W wolnym czasie zajmuję się fotografią, uprawiam sporty wodne(sufring) oraz pogłębiam wiedzę w zakresie kultury krajów azjatyckich.